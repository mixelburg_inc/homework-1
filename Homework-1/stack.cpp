#include "stack.h"
#include <iostream>

void push(stack* s, unsigned int element) {
	/**
	 * @brief adds new element to the stack
	 * @param s pointer to the stack
	 * @param element 
	*/
	add(&s->head, element);
	s->count++;
}

int pop(stack* s) {
	/**
	 * @brief removes element from the stack and returns it's value (-1 if the stack is empty)
	 * @param s pointer to the stack
	 * @return value of the element (or -1)
	*/
	if (s->head) {
		int rValue = s->head->value;
		s->head = remove(s->head);
		s->count--;

		return rValue;
	}
	else {
		return -1;
	}
}

void initStack(stack* s) {
	/**
	 * @brief initializes the stack
	 * @param s pointer to the stack
	*/
	s->count = 0;
	s->head = NULL;
}

void cleanStack(stack* s) {
	/**
	 * @brief cleans the stack
	 * @param s pointer to the stack
	*/
	deleteList(&s->head);
}
